# Functions provided in this file are used for displaying and storing graph data
from pyvis.network import Network
import webbrowser
import colorsys

# Define a location for storing the html file
default_storage_path = "graph.html"

# normalize data in the range 0 to 1
def normalize_data(measures, desired_max):
    key_max = max(measures.keys(), key=(lambda k: measures[k]))
    key_min = min(measures.keys(), key=(lambda k: measures[k]))
    min_val = measures[key_min]
    max_val = measures[key_max]

    for key in measures:
        try:
            measures[key] = ((measures[key] - min_val) / (max_val - min_val)) * desired_max
        except:
            measures[key] = 1 * desired_max # division by zero

# invert values in range(0, max_val)
def invert_values(measures, max_val):
    for key in measures:
        measures[key] = abs(measures[key] - max_val)
        
# adapted from https://stackoverflow.com/a/48288173
def RGBtoHex(vals):
    """Converts RGB values to Hex values.

        @param  vals     An RGB/RGBA tuple

        @return A hex string in the form '#RRGGBB' or '#RRGGBBAA'
    """

    if len(vals)!=3 and len(vals)!=4:
        raise Exception("RGB or RGBA inputs to RGBtoHex must have three or four elements!")

    vals = [255*x for x in vals]

    #Ensure values are rounded integers, convert to hex, and concatenate
    return '#' + ''.join(['{:02X}'.format(int(round(x))) for x in vals])

# Color graph according to given measure
def color_graph(graph, measures):
    normalize_data(measures, 0.708)
    invert_values(measures, 0.708) # so that blue=min, red=max

    for head in measures:
        graph.add_node(head, color=RGBtoHex(colorsys.hsv_to_rgb(measures[head], 1, 1)))

# Display the graph in the default browser
def display_graph(path=default_storage_path):
    webbrowser.open(path)

# Save the graph in a .html file
def save_graph_to_html(graph, path=default_storage_path):
    net = Network(notebook=True)
    net.from_nx(graph)
    net.show(path)


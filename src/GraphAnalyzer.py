from wikipedia import wikipedia
import networkx as nx
import random
from func_timeout import func_timeout, FunctionTimedOut
import numpy as np

# Function used for finding out whether wiki-page is about a person
def is_person(edge_head):
    try:
        """
        categories = wikipedia.page(edge_head).categories
        if 'Living people' in categories:
            print("Living person found: " + edge_head)
            return True
        else:
            print(edge_head + " is no living Person.")
            return False
        """
        return 'Living people' in wikipedia.page(edge_head).categories
    except:
        return False


def is_person_funny_version(edge_head):  # not used anymore, but did work surprisingly well
    try:
        return "born" in wikipedia.summary(edge_head)[0:100]
    except:
        return False


# Function used for parsing a wikipedia page and its links
# The function build_graph is used recursively to build up the graph
def generate_graph(start, end, max_links_per_node, max_depth):
    graph = nx.Graph()

    graph.add_node(start, color='#ff00d0') #add this here so that starting node can be a different color
    graph.add_node(end, color='#ff00d0')
    
    print("Stage 1: Calculating tree for " + start)
    graph_stage1 = build_graph(graph, [start], end, max_links_per_node, max_depth, [])

    print("Stage 2: Calculating tree for " + end)

    graphfinal = build_graph(graph_stage1, [end], start, max_links_per_node, max_depth, [])  # finished graph, just want to change some colors

    # color connecting edges:
    if nx.has_path(graphfinal, start, end):  # if person1 and person2 are connected I want to color the edges between them
        shortestpaths = dict(nx.all_pairs_shortest_path(graphfinal))  # get all shortest paths
        pathlengths = dict(nx.all_pairs_shortest_path_length(graphfinal))  # get lengths of all shortest paths
        for i in range(0, pathlengths[start][end]):  # change color of all edges on shortest connection between person1 and person2
            graphfinal.add_edge(shortestpaths[start][end][i], shortestpaths[start][end][i + 1], color="#ff00d0", width=8)  # change the color by re-adding the edges
    else:
        print("No connection between ", start, " and ", end, " was found.")

    return graphfinal


def build_graph(graph, edge_heads, end, max_links_per_node, remaining_depth, already_searched=[]):
    # Stop recursion, if already reached the max graph depth
    if remaining_depth <= 0:
        return graph

    # Filter out already traversed nodes
    edge_heads_filter = filter(lambda query: query not in already_searched, edge_heads)

    timeout = 2  # timeout in seconds, used to skip slow loading or inaccessible sites

    for head in edge_heads_filter:
        edge_tails_persons = []

        # Set this node as already searched
        already_searched.append(head)

        # Get all links for a wikipedia site
        # Ignore all inaccessible sites
        try:
            # edge_tails = wikipedia.page(head).links  # replace so that slow loading pages can be skipped:
            edge_tails_temp = func_timeout(timeout, wikipedia.page, args=(head,))
            edge_tails = edge_tails_temp.links

            # take only a random sample of links - amount determined by <max_links_per_node>
            if len(edge_tails) > max_links_per_node:  # only take random sample if we have more links than we need
                edge_tails = random.sample(edge_tails, max_links_per_node)
            
            # Filter out non-persons
            edge_tails_persons = list(filter(lambda query: is_person(query), edge_tails))

        except FunctionTimedOut:
            print("'wikipedia.page(" + str(head) + ")' could not complete within " + str(timeout) + " seconds and was skipped.")
        #except:
        except Exception as e:
            print("An error occurred at the wikipedia page " + str(head) + ": ", e)

        # Add edges to the graph
        for tail in edge_tails_persons:
            # For further information see: https://networkx.org/documentation/stable/reference/classes/generated/networkx.Graph.add_edge.html    
            graph.add_edge(head, tail, color='#000000')  # this changes the color of all the lines between the nodes (RGB)

            if tail == end:
                # remaining_depth = 0 #we could abort here or just finish all calculations
                print("Great! Connection to " + end + " was found. The rest of the graph will still be calculated.")

        # Call function recursively, decrement remaining_depth
        graph = build_graph(graph, edge_tails_persons, end, max_links_per_node, remaining_depth - 1, already_searched)

    return graph

# Create a text-based graph summary
def generate_graph_summary(graph, person_from, person_to):
    try:
        # https://networkx.org/documentation/latest/tutorial.html

        # total number of calculated nodes:
        number_of_nodes = nx.number_of_nodes(graph)

        # distance between person1 and person2:
        if nx.has_path(graph, person_from, person_to):
            distances = dict(nx.all_pairs_shortest_path_length(graph))
            s = "Distance between the two persons: " + str(distances[person_from][person_to])
        else:
            s = "No connection between the two persons was found."

        # Return interesting information in text:
        return "Number of nodes: " + str(number_of_nodes) + "\n" + s
    except Exception as e:
        print(e)
        return "An error occurred"

# Functions for calculating certain centralities for given graph
def get_degree_centrality(graph):
    print("Using degree centrality measure.")
    return nx.algorithms.centrality.degree_centrality(graph)

def get_closeness_centrality(graph):
    print("Using closeness centrality measure.")
    return nx.algorithms.centrality.closeness_centrality(graph)

def get_betweenness_centrality(graph):
    print("Using betweenness centrality measure.")
    return nx.algorithms.centrality.betweenness_centrality(graph)

def get_eigenvector_centrality(graph):
    print("Using eigenvector centrality measure.")
    return nx.algorithms.centrality.eigenvector_centrality(graph)

def get_katz_centrality(graph):
    print("Using katz centrality measure.")
    return nx.algorithms.centrality.katz_centrality(graph)

def get_harmonic_centrality(graph):
    print("Using harmonic centrality measure.")
    return nx.algorithms.centrality.harmonic_centrality(graph)
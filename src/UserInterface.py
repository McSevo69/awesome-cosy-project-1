# Import UI elements
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from multiprocessing.pool import ThreadPool  # the "threading" module does not allow return values this is used instead

# Import Utility classes
import GraphAnalyzer
import GraphVisualizer


def show_graph():
    GraphVisualizer.display_graph()


centrality_options = {
    "Degree Centrality": GraphAnalyzer.get_degree_centrality,
    "Closeness Centrality": GraphAnalyzer.get_closeness_centrality,
    "Betweenness Centrality": GraphAnalyzer.get_betweenness_centrality,
    "Eigenvector Centrality": GraphAnalyzer.get_eigenvector_centrality,
    "Katz Centrality": GraphAnalyzer.get_katz_centrality,
    "Harmonic Centrality": GraphAnalyzer.get_harmonic_centrality
}


# UI class with all the visible elements
class UserInterface(object):

    # Constructor of the main ui object
    def __init__(self, main_window):
        self.main_window = main_window
        self.centralwidget = QWidget(main_window)
        self.start_button = QPushButton(self.centralwidget)
        self.show_graph_button = QPushButton(self.centralwidget)
        self.first_person_field = QLineEdit(self.centralwidget)
        self.second_person_field = QLineEdit(self.centralwidget)
        self.first_person_label = QLabel(self.centralwidget)
        self.second_person_label = QLabel(self.centralwidget)
        self.result_label = QLabel(self.centralwidget)
        self.status_label = QLabel(self.centralwidget)
        self.graph_summary = QLabel(self.centralwidget)
        self.status_value = QLabel(self.centralwidget)
        self.graph_depth_field = QLineEdit(self.centralwidget)
        self.graph_depth_label = QLabel(self.centralwidget)
        self.links_per_node_label = QLabel(self.centralwidget)
        self.links_per_node_field = QLineEdit(self.centralwidget)
        self.centrality_choice_label = QLabel(self.centralwidget)
        self.centrality_choice = QComboBox(self.centralwidget)
        self.statusbar = QStatusBar(main_window)
        self.graph = None

    # Define all the ui elements
    def setup_ui(self):
        if not self.main_window.objectName():
            self.main_window.setObjectName(u"main_window")
        self.main_window.resize(530, 235)

        self.centralwidget.setObjectName(u"centralwidget")
        self.start_button.setObjectName(u"start_button")
        self.start_button.setGeometry(QRect(400, 10, 111, 51))
        self.show_graph_button.setObjectName(u"show_graph_button")
        self.show_graph_button.setGeometry(QRect(400, 160, 111, 51))
        self.first_person_field.setObjectName(u"first_person_field")
        self.first_person_field.setGeometry(QRect(140, 10, 161, 21))
        self.second_person_field.setObjectName(u"second_person_field")
        self.second_person_field.setGeometry(QRect(140, 50, 161, 21))
        self.first_person_label.setObjectName(u"first_person_label")
        self.first_person_label.setGeometry(QRect(20, 10, 91, 16))
        self.second_person_label.setObjectName(u"second_person_label")
        self.second_person_label.setGeometry(QRect(20, 50, 111, 16))
        self.status_label.setObjectName(u"status_label")
        self.status_label.setGeometry(QRect(420, 80, 51, 16))
        self.graph_summary.setObjectName(u"graph_summary")
        self.graph_summary.setGeometry(QRect(20, 150, 360, 64))
        self.status_value.setObjectName(u"status_value")
        self.status_value.setGeometry(QRect(420, 100, 80, 16))
        self.graph_depth_field.setObjectName(u"graph_depth_field")
        self.graph_depth_field.setGeometry(QRect(140, 90, 31, 21))
        self.graph_depth_label.setObjectName(u"graph_depth_label")
        self.graph_depth_label.setGeometry(QRect(20, 90, 91, 21))
        self.links_per_node_label.setObjectName(u"links_per_node_label")
        self.links_per_node_label.setGeometry(QRect(20, 120, 110, 21))
        self.links_per_node_field.setObjectName(u"links_per_node_field")
        self.links_per_node_field.setGeometry(QRect(140, 120, 31, 21))
        self.centrality_choice_label.setGeometry(QRect(190, 90, 140, 21))
        self.centrality_choice.setGeometry(QRect(190, 120, 165, 21))

        # Set up centrality combo box options
        for option in centrality_options.keys():
            self.centrality_choice.addItem(option)

        # Connect update function to combo box
        self.centrality_choice.currentTextChanged.connect(self.update_centrality_measurement)

        self.main_window.setCentralWidget(self.centralwidget)
        self.statusbar.setObjectName(u"statusbar")
        self.main_window.setStatusBar(self.statusbar)

        # Add button functionality
        self.connect_buttons()
        self.refresh_ui()

        # Set status to ready
        self.status_value.setText("Ready")

        QMetaObject.connectSlotsByName(self.main_window)

    # Refresh the ui
    def refresh_ui(self):
        self.main_window.setWindowTitle(QCoreApplication.translate("main_window", u"Wiki Graphs", None))
        self.start_button.setText(QCoreApplication.translate("main_window", u"Start", None))
        self.show_graph_button.setText(QCoreApplication.translate("main_window", u"Show Graph", None))
        self.first_person_label.setText(QCoreApplication.translate("main_window", u"First person:", None))
        self.second_person_label.setText(QCoreApplication.translate("main_window", u"Second person:", None))
        self.status_label.setText(QCoreApplication.translate("main_window", u"Status:", None))
        self.graph_summary.setText("")
        self.status_value.setText("")
        self.graph_depth_field.setText(QCoreApplication.translate("main_window", u"3", None))
        self.graph_depth_label.setText(QCoreApplication.translate("main_window", u"Graph depth:", None))
        self.links_per_node_label.setText(QCoreApplication.translate("main_window", u"Links per node:", None))
        self.links_per_node_field.setText(QCoreApplication.translate("main_window", u"10", None))
        self.centrality_choice_label.setText(QCoreApplication.translate("main_window", u"Centrality measure:", None))

    # Add button functionality
    def connect_buttons(self):
        self.show_graph_button.clicked.connect(show_graph)
        self.start_button.clicked.connect(self.start_search)

    def update_centrality_measurement(self, centrality_measure_choice):
        if self.graph is not None:
            print("Centrality changed to " + centrality_measure_choice + "... Recoloring...")
            # Recalculate centrality measurement
            centrality_measures = centrality_options[centrality_measure_choice](self.graph)

            # Recoloring graph
            GraphVisualizer.color_graph(self.graph, centrality_measures)  

            # Updating HTML file  
            GraphVisualizer.save_graph_to_html(self.graph)
            print("Graph successfully updated. Press Show Graph or reload HTML page!")


    # Start button was clicked -> A graph will be created
    def start_search(self):
        print("Processing...")
        # Set the state to analyzing
        self.graph_summary.setText("")  # remove summary from previous graph
        self.status_value.setText(
            "Analyzing...")  # this alone does not work because the method is blocking the GUI (?), so:
        self.status_value.repaint()  # so we also need this line

        # Get values from the ui
        person1 = self.first_person_field.text()
        person2 = self.second_person_field.text()
        graph_depth = int(self.graph_depth_field.text())
        links_per_node = int(self.links_per_node_field.text())

        # Add quotation marks to start/end nodes so that they are found on Wikipedia more likely
        person1_quoted = '"' + person1 + '"'
        person2_quoted = '"' + person2 + '"'

        # Create and save the graph
        pool = ThreadPool(processes=1)  # threading
        async_result = pool.apply_async(GraphAnalyzer.generate_graph, (
        person1_quoted, person2_quoted, links_per_node, graph_depth))  # call Graph Analyzer in new Thread
        self.graph = async_result.get()  # get result from thread

        # Change node sizes and remove quotes
        self.graph.nodes[person1_quoted]['size'] = 25
        self.graph.nodes[person2_quoted]['size'] = 25
        self.graph.nodes[person1_quoted]['label'] = person1
        self.graph.nodes[person2_quoted]['label'] = person2

        # Color graph according to chosen centrality measurement
        centrality_measure_choice = self.centrality_choice.currentText()
        centrality_measures = centrality_options[centrality_measure_choice](self.graph)
        GraphVisualizer.color_graph(self.graph, centrality_measures)        

        # Create HTML file and save it to given path
        GraphVisualizer.save_graph_to_html(self.graph)

        # Set the state to finished
        self.status_value.setText("Finished")
        print("Finished.")

        # Create and display graph summary
        self.graph_summary.setText(str(GraphAnalyzer.generate_graph_summary(self.graph, person1_quoted, person2_quoted)))

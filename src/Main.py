# This is the entry point for the program -> See main method

# Import ui libraries and the UserInterface class
import sys
from PySide2 import QtWidgets
from PySide2.QtWidgets import QApplication
from UserInterface import UserInterface


# Main method
def main():
    # Instantiate the user interface and launch application
    app = QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = UserInterface(main_window)
    ui.setup_ui()
    main_window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
